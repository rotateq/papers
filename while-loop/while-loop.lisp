(cl:defpackage #:loops
  (:use #:cl)
  (:shadow #:when)
  (:local-nicknames (#:a #:alexandria))
  (:export #:while))

(cl:in-package #:loops)

#+(or) ; Please ignore this line.
(let ((nums1 '(1 2))
      (nums2 '(2 3)))
  `(+ ,@(union nums1 nums2 :test #'=)))

(defmacro when (test &body body)
  `(if ,test
       (progn
         ,@body)))

(defmacro buggy-when (test &body body)
  `(if ,test
       ,body))

(defmacro static-while (condition &body body)
  `(block while-loop
     (tagbody
      begin
        (when ,condition
          ,@body
          (go begin))
      end
        (return-from while-loop))))

(defmacro while (condition &body body)
  (a:with-gensyms (begin end while-loop)
    `(block ,while-loop
       (tagbody
        ,begin
          (when ,condition
            ,@body
            (go ,begin))
        ,end
          (return-from ,while-loop)))))

#+(or) ; Please ignore this line.
(let ((i 0)
      (sum 0))
  (declare (type (integer 0) i sum))
  (while (< i 5)
    (incf sum i)
    (incf i))
  sum)
